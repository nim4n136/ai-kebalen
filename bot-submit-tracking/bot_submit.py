import requests
import json
from glob import glob
import random
from datetime import datetime as dt
import base64

class BotSubmitHealtTracking():

    data_dir = ""

    data_not_valid = False

    list_nik = []

    headers = {
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache',
            'Accept': 'application/json, text/plain, */*',
            'Sec-Fetch-Dest': 'empty',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            'DNT': '1',
            'Origin': 'https://healthtracking.digitalevent.id',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Referer': 'https://healthtracking.digitalevent.id/check',
            'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7,ms;q=0.6',
        }

    def __init__(self):
        pass

    def run(self):
        if self.data_not_valid:
            print("Data not valid, {}".format(self.data_not_valid))
            return
        for directory in glob("./{}/*".format(self.data_dir)):
            nik = self.nikFromDir(directory)
            self.list_nik.append(nik)

        # Ambil nik secara acak dan ambil nik yang belum di submit
        shuffle_nik = random.shuffle(self.list_nik)
        nik_to_submit = False
        for nik in self.list_nik:
            if self.historyCheck(nik) == True:
                continue
            nik_to_submit = nik
            break

        if nik_to_submit == False:
            return

        config_parse = self.parseConfig(nik=nik_to_submit,directory=self.data_dir)
        self.generateDataPost(config=config_parse)


    def generateDataPost(self, config):
        open_template = open("./template.json").read()
        json_template = json.loads(open_template)
        json_template['full_name'] = config['full_name']
        json_template['nik'] = config['nik']
        json_template['suhu'] = self.randomList(config['suhu'])
        json_template['create_lat'] = self.randomList(config['lat'])
        json_template['create_long'] = self.randomList(config['lon'])
        photoBase64 = self.image2base64(self.randomImage(config['nik']))
        json_template['suhu_photo'] = photoBase64
        user_agent = config['user_agent']
        self.submitHealtTracking(data=json_template, user_agent=user_agent)


    def submitHealtTracking(self, data, user_agent):
        url_regis = "https://healthtreck.digitalevent.id/api/employeeregis"
        payload = json.dumps([data])
        edit_header = {
          'User-Agent': user_agent,
          'Content-Type': 'application/json;charset=UTF-8'
        }
        headers = {**self.headers, **edit_header}
        response = requests.request("POST", url=url_regis, headers=headers, data=str(payload))
        logs = open("logs.txt","a+")
        logs.write(data['nik']+" => "+str(response.text.encode('utf8'))+"\n")
        logs.close()

    """
        history Check
    """
    def historyCheck(self, nik):
        date = dt.now().strftime("%Y-%m-%d")
        url_history_check = "https://healthtreck.digitalevent.id/api/employeehistory?nik={nik}&date={date}".format(
            date=date,
            nik=nik
            )
        response = requests.request('GET', url=url_history_check, headers=self.headers)
        response = str(response.text.encode('utf8'))
        if date in response:
            return True
        return False


    """
        Random data
    """
    def randomList(self, dataList):
        length = len(dataList) - 1
        ran = random.randint(0, length)
        return dataList[ran]


    """ 
        Convert image to base64
    """
    def image2base64(self, imge_location):
        with open(imge_location, "rb") as image_file:
            data = base64.b64encode(image_file.read())
            data = data.decode("utf-8")
            format_image = "data:image/jpeg;base64,"
            return format_image+data

    """ 
        Random image
    """
    def randomImage(self, nik):
        lst_photos = glob("{dir}/{nik}/photos/*".format(nik=nik, dir=self.data_dir))
        return self.randomList(lst_photos)

    """
        Setup data & checking config
    """
    def setup(self, data_dir = "data"):
        self.data_dir = data_dir
        try:
            self.checkDataValid()    
        except Exception as e:
            self.data_not_valid = str(e)

    """
        Checking data is valid
    """
    def checkDataValid(self):
        for directory in glob("./{}/*".format(self.data_dir)):
            nik = self.nikFromDir(directory)
            if not self.checkNik(nik):
                raise Exception("NIK {} Tidak valid ".format(nik))
            else:
                config = self.parseConfig(nik=nik, directory=self.data_dir)
                self.checkingConfig(config)
                self.checkingPhotos(nik)

    """
        parse config from file
    """
    def parseConfig(self,nik, directory):
        read_config = open("{directory}/{nik}/config.json".format(directory=directory,nik=nik)).read()
        convert_json = json.loads(read_config)
        return convert_json

    """
        Checking file config
    """
    def checkingConfig(self, config_json):
        def is_empty(any_structure):
            if any_structure:
                return False
            return True

        key_config = ["full_name", "nik", "lat", "lon", "suhu", "user_agent"]
        must_type_list = ["lat", "lon", "suhu"]
        for key in key_config:
            if key not in config_json:
                raise Exception("Data {key} tidak ada di config".format(key=key), config_json)

            if is_empty(config_json[key]) == True:
                raise Exception("Data {key} masih kosong?".format(key=key), config_json)

            if key in must_type_list:
                if type(config_json[key]) != list:
                    raise Exception("Data {key} harus berupa array ".format(key=key), config_json)


    """
        Photos checking
    """
    def checkingPhotos(self, nik):
        lst_photos = glob("{dir}/{nik}/photos/*.jpeg".format(nik=nik, dir=self.data_dir))
        if len(lst_photos) < 3:
            total = len(lst_photos)
            raise Exception("Foto {nik} saat ini {total}, foto harus di masukan minimal 3".format(nik=nik, total=total))

    """
        Get information nik from directory name
    """
    def nikFromDir(self, directory):
        directory = directory.replace("\\","/")
        sp = directory.split("/")
        return sp[2]


    """
        Checking nik
    """
    def checkNik(self ,nik):
        nik_checking_url = "https://healthtreck.digitalevent.id/api/masteremployee?nik={}".format(nik)
        checking_nik = requests.request('GET', url=nik_checking_url, headers=self.headers)
        result = str(checking_nik.text.encode('utf8'))
        if nik in result:
            return True
        return False



        
