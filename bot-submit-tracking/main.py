from bot_submit import BotSubmitHealtTracking
from random import randint
import time
from datetime import datetime as dt
import datetime


def start_bot():
    bot = BotSubmitHealtTracking()
    bot.setup(data_dir="data")
    bot.run()

# Mulai pada jam
start_time = datetime.time(0, 47)

# Selesai pada jam
expire_time = datetime.time(11, 0)

while True:
    time_now = dt.now().time()

    # jika waktu sudah habis
    if time_now >= expire_time:
        print("Habis", expire_time)
        continue

    # jika waktu sudah di mulai
    if time_now >= start_time:
        print("Mulai", time_now)
        random = randint(100,200)
        print(random)
        time.sleep(random)
        start_bot()
        break
