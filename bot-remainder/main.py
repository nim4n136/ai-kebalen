import os
import json
import time
import logging
import telegram
import requests
import schedule
from datetime import datetime as dt
from dotenv import load_dotenv
load_dotenv()

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    filename="logging.log")

bot = telegram.Bot(token=os.getenv('TELEGRAM_BOT_TOKEN'))

class RemainderCheck():

    has_finish = 0

    id_group = os.getenv('ID_GROUP')

    headers = {
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache',
            'Accept': 'application/json, text/plain, */*',
            'Sec-Fetch-Dest': 'empty',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            'DNT': '1',
            'Origin': 'https://healthtracking.digitalevent.id',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Referer': 'https://healthtracking.digitalevent.id/myhistory',
            'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7,ms;q=0.6',
        }

    def history_check(self, nik):
        date = dt.now().strftime("%Y-%m-%d")
        url_history_check = "https://healthtreck.digitalevent.id/api/employeehistory?nik={nik}&date={date}".format(
            date=date,
            nik=nik
            )
        response = requests.request('GET', url=url_history_check, headers=self.headers)
        response = str(response.text.encode('utf8'))
        if date in response:
            return True
        return False


    def not_yet_submit(self):
        lst_nik = json.loads(open('list_nik.json').read())
        yang_belum = ""
        for orang in lst_nik:
            check = self.history_check(nik=orang['nik'])
            if not check:
                yang_belum +="<b>{}</b>\n".format(orang['full_name'])
        if yang_belum == "":
            if self.has_finish:
                return
            self.has_finish = 1
            bot.send_message(chat_id=self.id_group, text="Mantaps!, semuanya sudah isi Healthcheck")
            return 
        self.has_finish = 0
        bot.send_message(chat_id=self.id_group, text="Jangan lupa Healthchecknya gaes \n Ayo yang belum nih:")
        bot.send_message(chat_id=self.id_group, text=yang_belum, parse_mode="html")

check_remaind = RemainderCheck()

def check():
    check_remaind.not_yet_submit()

time_sc = [
    "07:30",
    "08:00",
    "08:30",
    "09:00",
    "09:30",
    "10:00",
    "10:05",
    "10:10",
    "10:15",
    "10:30",
    "10:35",
    "10:40",
    "10:45",
    "10:50",
    "10:55",
    "10:58"
]

for t in time_sc:
    schedule.every().day.at(t).do(check)

while True:
    schedule.run_pending()
    time.sleep(1)